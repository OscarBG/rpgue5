Risketos Basicos
- Un sistema preparat per tenir diverses classes, només se'n requereix un però ha d'estar preparat per ampliar, per exemple mag. En aquest ha de ser l'estructura de dades per a les estadístiques del jugador, vida, manà, energia, dany físic, dany màgic, etc. Això és al gust de cada joc. - SI
- Un sistema d'habilitats que permeti al jugador canviar quina habilitat fer servir, ha de tenir com a mínim 2. - SI
- Un sistema de combat que et permeti atacar, llançar habilitats i rebre mal. - SI pero recibir daño no, si me da la vida lo pongo
- Sistema d'anivellament que faci pujar la teva experiència i nivell, ia cada nivell pugin les teves estadístiques - SI


Risketos Opcionals (30%)
- Sistema d’inventari amb objectes que deixen els enemics. -NO 
- Relacionat amb l’anterior un sistema d’equipació que modifiqui les teves estadístiques. - NO
- Mapa o minimapa que mostras a on estàs. - NO
- Sistema d’animacions avançat amb BlendTrees (recomano utilizar mixamo) - SI

