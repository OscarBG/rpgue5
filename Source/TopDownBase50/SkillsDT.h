// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "SkillsDT.generated.h"

UENUM(BlueprintType)
enum ESkillCastType {
	OnLocation,
	OnDirection
};

/**
 * 
 */
USTRUCT(BlueprintType)
struct TOPDOWNBASE50_API FSkillsDT : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FString name;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TSubclassOf<class ASpell> skillBP;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int manaCost;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float duration;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int baseDamage;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool proc;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int procDamage;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool slows;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TEnumAsByte<ESkillCastType> castType;

	FSkillsDT() : name(""), skillBP(nullptr), manaCost(0), duration(0.f), baseDamage(0),
			proc(false), procDamage(0), slows(false), castType(OnLocation){}
	
};
