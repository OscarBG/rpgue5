// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SkillsDT.h"
#include "GameFramework/Actor.h"
#include "Spell.generated.h"

UCLASS()
class TOPDOWNBASE50_API ASpell : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpell();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FVector spawnLoc;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float duration;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int baseDamage;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int procDamage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AActor* target;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector targetLoc;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FRotator targetDir;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TEnumAsByte<ESkillCastType> castType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float damageModifier;

	void SetDamage(int dmg) { baseDamage = dmg;}

	UFUNCTION(BlueprintCallable)
	void Initialize(int _dmg, int _procDmg, float _durat, AActor* _target, FVector _targetLoc,
		FRotator _targetDir, TEnumAsByte<ESkillCastType> _castType, float _dmgMod )
	{
		baseDamage = _dmg;
		procDamage = _procDmg;
		duration = _durat;
		target = _target;
		targetLoc = _targetLoc;
		targetDir = _targetDir;
		castType = _castType;
		damageModifier = _dmgMod;
		
	}
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
